from silx.utils.enum import Enum as _Enum


class DatasetScope(_Enum):
    LOCAL = "local"
    GLOBAL = "global"
