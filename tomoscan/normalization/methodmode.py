from silx.utils.enum import Enum as _Enum


class MethodMode(_Enum):
    SCALAR = "scalar"
    POLYNOMIAL_FIT = "polynomial fit"
