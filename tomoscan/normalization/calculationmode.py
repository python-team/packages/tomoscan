from silx.utils.enum import Enum as _Enum


class CalculationMode(_Enum):
    MEAN = "mean"
    MEDIAN = "median"
