# tomoscan

This library offers an abstraction to:

* access tomography data from spec acquisitions (EDF) and bliss acquisitions (HDF5)
* read and write volumes from / to HDF5, JP2K, TIFF and EDF


## installation


### using pypi

To install the latest 'tomoscan' pip package

``` bash
pip install tomoscan
```

### using gitlab repository

``` bash
pip install git+https://gitlab.esrf.fr/tomotools/tomoscan.git
```

## documentation

General documentation can be found here: [https://tomotools.gitlab-pages.esrf.fr/tomoscan/](https://tomotools.gitlab-pages.esrf.fr/tomoscan/)
